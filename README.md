# Keyboard Knob
A ATMEGA32U4 based keyboard emulator knob

## General Requirements
- USB powered
- Windows compatible
- Keyboard/Mouse Emulator
- Rotatry dial
- Reprogram and reconfigure -able

## Side Goals
- [x] Create project in Git
- [ ] Add Git hash to Altium Templates
- [ ] TBD


## References
- [Sparkfun Turn your ProMicro into a USB Keyboard/Mouse](https://www.sparkfun.com/tutorials/337)
- [How Rotary Encoder Works and Interface It with Arduino](https://lastminuteengineers.com/rotary-encoder-arduino-tutorial/)
- [USB Volume Knob](https://www.instructables.com/USB-Volume-Knob/)


---
## Tag History
| TAG | DATE | CHANGES | 
|:---:|:----:|:------- |
| XX | NA | None | 
